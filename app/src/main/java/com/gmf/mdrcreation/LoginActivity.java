package com.gmf.mdrcreation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.support.design.widget.Snackbar;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;

//import android.content.CursorLoader;
//import android.content.Loader;
//import android.database.Cursor;
//import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
//import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
//import android.widget.ArrayAdapter;
//import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Wave;
import com.gmf.mdrcreation.util.ConstantVar;
import com.gmf.mdrcreation.model.ResponseApi;
import com.gmf.mdrcreation.network.RetrofitClientInstance;
import com.gmf.mdrcreation.service.ApiService;
import com.google.gson.internal.LinkedTreeMap;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends AppCompatActivity{

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    SharedPreferences sharedpreferences;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    PrettyDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.

        mUsernameView = (EditText) findViewById(R.id.username);

//        String reqString = Build.MANUFACTURER; //merek
//                //Build.MODEL  //Tipe Hape
//                //Build.VERSION.RELEASE //Android version
//                //Jenis Android
//                //Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

        //populateAutoComplete();

        //setup loading
        mLoadingFrm = (RelativeLayout) findViewById(R.id.loadingfrm);
        Wave loadingstyle = new Wave();
        loadingstyle.setColor(Color.WHITE);
        mLoadingBar = (ProgressBar) findViewById(R.id.loadingbar);
        mLoadingBar.setIndeterminateDrawable(loadingstyle);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        //mProgressView = findViewById(R.id.login_progress);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            loginUser();

//            mAuthTask = new UserLoginTask(username, password);
//            mAuthTask.execute((Void) null);
        }
    }

    private void loginUser(){
        try {

            String manufacture = Build.MANUFACTURER; //merek
            String model = Build.MODEL;  //Tipe Hape
            String version = Build.VERSION.RELEASE; //Android
            //Jenis Android
            String sdk = Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getLoginUser(
                    mUsernameView.getText().toString(),
                    mPasswordView.getText().toString(),
                    manufacture,
                    model,
                    version,
                    sdk
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){

                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
                            Object s = (Object) response.body().getResultdata();
                            LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                            map = (LinkedTreeMap<String, String>) s;
//                            Editor editor = new Edito
                            sharedpreferences = getApplicationContext().getSharedPreferences(
                                    ConstantVar.PREF_SESSION, 0
                            );
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(ConstantVar.PREF_USERNAME_KEY, mUsernameView.getText().toString());
                            editor.putString(ConstantVar.PREF_EMAIL_KEY, map.get("email"));
                            editor.putString(ConstantVar.PREF_NAME_KEY, map.get("nama"));
                            editor.putString(ConstantVar.PREF_USERDESC_KEY, map.get("jabatan"));
                            editor.putString(ConstantVar.PREF_ACCOUNT, map.get("account_name"));
                            editor.putString(ConstantVar.PREF_TOKEN_KEY, map.get("token"));
                            editor.commit();

//                             System.out.println(map.get("displayname"));
//                             System.out.println(map.get("description"));

                            Intent k = new Intent(getApplicationContext(), MainActivity.class);
//                            k.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            k.putExtra("EXIT",true);
                            showProgress(false);
                            startActivity(k);
                            finish();
                        }else{
                            String err = response.body().getMessage();
                            showProgress(false);
                            new PrettyDialog(LoginActivity.this)
                                    .setTitle("Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        Log.d("Retro",response.message());
                        showProgress(false);
                        new PrettyDialog(LoginActivity.this)
                                .setTitle("Failed")
                                .setMessage(response.message())
                                .show();

                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    showProgress(false);
                    //Log.d("Retro",t.getMessage().toString());
                    new PrettyDialog(LoginActivity.this)
                            .setTitle("Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });

        } catch(Exception e) {
            showProgress(false);
            new PrettyDialog(LoginActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Check Your Connection")
                    .show();
            e.printStackTrace();
        }
    }


    private boolean isPasswordValid(String password) {
        // Replace this with your own logic
        return password.length() > 2;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mLoadingFrm.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoadingFrm.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoadingFrm.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoadingFrm.setVisibility(show ? View.VISIBLE : View.GONE);
            //mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            //  attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

