package com.gmf.mdrcreation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gmf.mdrcreation.util.ConstantVar;

public class MainActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    TextView tvaccount,tvfullname,tvjobdesk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        LinearLayout mcreate = (LinearLayout) findViewById(R.id.btncreatemdr);
        LinearLayout msummary = (LinearLayout) findViewById(R.id.btnsummary);

        //TextView txtcoba = (TextView) findViewById(R.id.testsession);

        tvaccount = (TextView) findViewById(R.id.tvaccount);
        tvfullname = (TextView) findViewById(R.id.tvfullname);
        tvjobdesk = (TextView) findViewById(R.id.tvjobdesk);


        sharedPreferences = getSharedPreferences(ConstantVar.PREF_SESSION,0);
//        String a = sharedPreferences.getString(ConstantVar.PREF_USERDESC_KEY,null);
//        txtcoba.setText(a);
        tvaccount.setText(sharedPreferences.getString(ConstantVar.PREF_EMAIL_KEY,null));
        tvfullname.setText(sharedPreferences.getString(ConstantVar.PREF_NAME_KEY,null));
        tvjobdesk.setText(sharedPreferences.getString(ConstantVar.PREF_USERDESC_KEY,null));

        mcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Intent k = new Intent(LoginActivity.this, MainActivity.class);
                    Intent k = new Intent(getApplicationContext(), MdrScanActivity.class);
                    startActivity(k);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });

        msummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Intent k = new Intent(LoginActivity.this, MainActivity.class);
                    Intent k = new Intent(getApplicationContext(), SummaryActivity.class);
                    startActivity(k);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    boolean doubleBackPressed = false;
    @Override
    public void onBackPressed() {
        //finish();
        if(doubleBackPressed){
            super.onBackPressed();
            getApplicationContext().getSharedPreferences(ConstantVar.PREF_SESSION, 0)
                    .edit().clear().commit();
            return;
        }
        this.doubleBackPressed = true;
        Toast.makeText(this, "Please Click BACK Again To Exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackPressed=false;
            }
        },2000);
    }

}
