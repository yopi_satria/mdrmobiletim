package com.gmf.mdrcreation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Wave;
import com.gmf.mdrcreation.model.ObjUploadImage;
import com.gmf.mdrcreation.service.BgJobImageUploadService;
import com.gmf.mdrcreation.util.ConstantVar;
import com.gmf.mdrcreation.adapter.DefectLimitAdapter;
import com.gmf.mdrcreation.util.Helper;
import com.gmf.mdrcreation.adapter.ImageListAdapter;
import com.gmf.mdrcreation.util.PopupCauseCode;
import com.gmf.mdrcreation.util.PopupMDRcode;
import com.gmf.mdrcreation.adapter.PsiAdapter;
import com.gmf.mdrcreation.adapter.StaTypeAdapter;
import com.gmf.mdrcreation.model.ObjDefectLimit;
import com.gmf.mdrcreation.model.ObjPSI;
import com.gmf.mdrcreation.model.ObjStaType;
import com.gmf.mdrcreation.model.ResponseApi;
import com.gmf.mdrcreation.network.RetrofitClientInstance;
import com.gmf.mdrcreation.service.ApiService;
import com.gmf.mdrcreation.util.PopupObjectPart;
import com.google.gson.internal.LinkedTreeMap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.concurrent.TimeUnit;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MdrFormActivity extends AppCompatActivity {
    //LinearLayout mHidenForm;
    List<String> imageList = new ArrayList<>();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    int galeri=0;
    Button mBtnSubmit, mBtnCancel, mBtnChoose, mBtnCapture;
    Button mBtnMdrCode, mBtnCauseCode, mBtnObjPart;
    //MyAdapter imgAdapter;
    Spinner mComboDefect,mComboPSI,mComboStatype;

    EditText mTxtDefectTitle, mTxtOprnRemake,
        mTxtStaFrom, mTxtStaTo,
        mTxtStringFrom, mTxtStringTo,
        mTxtWLFrom, mTxtWLTo,
        mTxtBLFrom, mTxtBLTo,
        mTxtClock,mTxtZone,
        mTxtCauseText
    ;
    PopupMDRcode popMdr;
    PopupCauseCode popCause;
    PopupObjectPart popObjpart;

    SharedPreferences mPrefSession;

    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mdr_form);

        mPrefSession = getSharedPreferences(ConstantVar.PREF_SESSION,0);

        //setup loading
        mLoadingFrm = (RelativeLayout) findViewById(R.id.loadingfrm);
        Wave loadingstyle = new Wave();
        loadingstyle.setColor(Color.WHITE);
        mLoadingBar = (ProgressBar) findViewById(R.id.loadingbar);
        mLoadingBar.setIndeterminateDrawable(loadingstyle);

        //define Text
        mTxtDefectTitle = findViewById(R.id.txtDefectTitle);
        mTxtOprnRemake = findViewById(R.id.txtRemake);
        mTxtStaFrom = findViewById(R.id.txtStaFrom);
        mTxtStaTo = findViewById(R.id.txtStaTo);
        mTxtStringFrom = findViewById(R.id.txtStringFrom);
        mTxtStringTo = findViewById(R.id.txtStringTo);
        mTxtWLFrom = findViewById(R.id.txtWLFrom);
        mTxtWLTo = findViewById(R.id.txtWLTo);
        mTxtBLFrom = findViewById(R.id.txtBLFrom);
        mTxtBLTo = findViewById(R.id.txtBLTo);
        mTxtClock = findViewById(R.id.txtClock);
        mTxtZone = findViewById(R.id.txtZone);
        mTxtCauseText = findViewById(R.id.txtCauseText);

        recyclerView = findViewById(R.id.listimage);
        //recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setHasFixedSize(true);

        mComboPSI = findViewById(R.id.combo1);
        mComboStatype = findViewById(R.id.combo2);
        mComboDefect = findViewById(R.id.combo3);

        //construct Combobox
        addItemsOnPSI();
        addItemsOnStationType();
        addItemsOnDefectLimit();

        mBtnMdrCode = findViewById(R.id.btnmdrcode);
        mBtnCauseCode = findViewById(R.id.btncausecode);
        mBtnObjPart = findViewById(R.id.btnobpart);

        mBtnChoose = (Button) findViewById(R.id.btnchoose);
        mBtnCapture = (Button) findViewById(R.id.btncapture);
        mBtnSubmit = (Button) findViewById(R.id.btnsubmit);
        mBtnCancel = (Button) findViewById(R.id.btncancel);

        popMdr = new PopupMDRcode(MdrFormActivity.this);
        popObjpart = new PopupObjectPart(MdrFormActivity.this);
        popCause = new PopupCauseCode(MdrFormActivity.this);


        mBtnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recyclerView.getChildCount()>4){
                    new PrettyDialog(MdrFormActivity.this)
                            .setTitle("Info")
                            .setMessage("Image Tidak Boleh Lebih Dari 5")
                            .show();
                    return;
                }

                galeri=2;
//                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(intent);
                if (ContextCompat.checkSelfPermission(MdrFormActivity.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MdrFormActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            0);
                }
                try{
                    cameraIntent();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        mBtnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recyclerView.getChildCount() > 4){
                    new PrettyDialog(MdrFormActivity.this)
                            .setTitle("Info")
                            .setMessage("Image Tidak Boleh Lebih Dari 5")
                            .show();
                    return;
                }
//                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(pickPhoto , 1);
                galeri=1;
                boolean result = Helper.checkPermission(MdrFormActivity.this);
                if(result)
                    galleryIntent();
            }
        });

        mBtnMdrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popMdr.popupdialog();
            }
        });
        mBtnObjPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popObjpart.popupdialog();
            }
        });
        mBtnCauseCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popCause.popupdialog();
            }
        });
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //insertMDR();
                //uploadimage();
                submitForm();
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent k = new Intent(getApplicationContext(), MdrScanActivity.class);
                startActivity(k);
                finish();
            }
        });
    }

    private void submitForm(){
        Log.d("aaaaa","mdr = "+popMdr.getM_mdrcode()+" dan "+popMdr.getM_subcode());
        Log.d("aaaaa","cause = "+popCause.getM_mdrcode()+" dan "+popCause.getM_subcode());
        Log.d("aaaaa","objpart = "+popObjpart.getM_mdrcode()+" dan "+popObjpart.getM_subcode());

        if(popMdr.getM_mdrcode() == null){
            new PrettyDialog(MdrFormActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Choose MDR Code")
                    .show();
            return;
        }
        if(TextUtils.isEmpty(mTxtDefectTitle.getText())){
            mTxtDefectTitle.setError("Please Fill Defect Title");
            mTxtDefectTitle.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(mTxtOprnRemake.getText())){
            mTxtOprnRemake.setError("Please Fill Operation Remark");
            mTxtOprnRemake.requestFocus();
            return;
        }
        if(popMdr.getM_mdrcode().equals("ZPM-STR")){
            if(TextUtils.isEmpty(mTxtStaFrom.getText())){
            }
            if(TextUtils.isEmpty(mTxtStaTo.getText())){
            }
            if(TextUtils.isEmpty(mTxtStringFrom.getText())){
            }
            if(TextUtils.isEmpty(mTxtStringTo.getText())){
            }
            if(TextUtils.isEmpty(mTxtWLFrom.getText())){
            }
            if(TextUtils.isEmpty(mTxtWLTo.getText())){
            }
            if(TextUtils.isEmpty(mTxtBLFrom.getText())){
            }
            if(TextUtils.isEmpty(mTxtBLTo.getText())){
            }
            if(TextUtils.isEmpty(mTxtClock.getText())){
            }
            if(TextUtils.isEmpty(mTxtZone.getText())){
            }
            if(TextUtils.isEmpty(mTxtCauseText.getText())){
                mTxtCauseText.setError("Please Fill Cause Text");
                mTxtCauseText.requestFocus();
                return;
            }
        }
        PrettyDialog pDialog = new PrettyDialog(MdrFormActivity.this);
        pDialog.setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.pdlg_color_gray)
                .setTitle("Are you sure to create MDR Order?")
                .addButton(
                        "OK",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.pdlg_color_green,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                                sendTrans();
                            }
                        }
                )
                .addButton(
                        "Cancel",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        }
                )
                .setAnimationEnabled(true)
                .show();
//        if(recyclerView.getChildCount()>0){
//            uploadImage();
//        }
    }

    private Uri photoUri;
    // add items into spinner dynamically
    void cameraIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            File file = null;
            try {
                file = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            photoUri = null;
            if (file != null) {
                photoUri = Uri.fromFile(file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, 2);
            }
        }
        //startActivityForResult(intent, 2);
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES +"/MDRCreationImage/");
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/MDRCreationImage/");
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +"/MDRCreation/images/");
        if(!storageDir.exists()) storageDir.mkdirs();

        return File.createTempFile(timeStamp, ".jpg", storageDir);
    }

    void galleryIntent(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Helper.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(galeri==1)
                        galleryIntent();
                    else if(galeri==2){
                        cameraIntent();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1)
                onSelectFromGalleryResult(data);
            else if (requestCode == 2){
                Log.d("zzzkamera",photoUri.getPath());
                imageList.add(photoUri.getPath());
                //mAdapter = new MyAdapter(imageList);
                mAdapter = new ImageListAdapter(getApplicationContext(), imageList);
                recyclerView.setAdapter(mAdapter);
                galleryAddPic();
                //onCaptureImageResult(data);
            }
        }
    }
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoUri.getPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


//    private void onCaptureImageResult(Intent image) {
//        Bitmap thumbnail = (Bitmap) image.getExtras().get("data");
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//        File destination = new File(Environment.getExternalStorageDirectory () +"/MDRCreation/images/");
//        if(!destination.exists()) destination.mkdirs();
//
////        String session = "g580422";
////        session.toUpperCase();
//        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        Long timestamp = System.currentTimeMillis();
//        String imageFileName =  timestamp.toString() + ".jpg";//session.toString() +
//
//        destination = new File(Environment.getExternalStorageDirectory () +"/MDRCreation/images/" +imageFileName);
//
//        String imagePath = destination.toString();
//
//        FileOutputStream fo;
//        try {
//
//            destination.createNewFile();
//            fo = new FileOutputStream(destination);
//            fo.write(bytes.toByteArray());
//            //fo.flush();
//            fo.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Log.d("zzzkamera",imagePath);
//        imageList.add(imagePath);
//        //mAdapter = new MyAdapter(imageList);
//        mAdapter = new ImageListAdapter(getApplicationContext(), imageList);
//        recyclerView.setAdapter(mAdapter);
//    }

//    private void saveMediaImage(Intent data){
//        Uri dataUri = data.getData();
//        if (dataUri != null) {
//            // Save Gallery image
//            //File mydir = getDir(“application_folder_name”, MODE_PRIVATE);
//            File destination = new File(Environment.getExternalStorageDirectory () +"/MDRCreation/images/");
//            if(!destination.exists()) destination.mkdirs();
//
//            String fileName = "image_" + System.currentTimeMillis() + ".jpg";
//            // File name
//            final File f = new File(destination.getAbsolutePath(), fileName);
//            String imagePath = f.getPath();
//
//            try {
//                String mSelectedImagePath = getPath(dataUri);
//                // Save the image from mSelectedImagePath path
//
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//            Log.d("zzzkamera",imagePath);
//            imageList.add(imagePath);
//            //mAdapter = new MyAdapter(imageList);
//            mAdapter = new ImageListAdapter(getApplicationContext(), imageList);
//            recyclerView.setAdapter(mAdapter);
//        }else{
//            // image is not in gallery
//            Toast.makeText(getApplicationContext(), "Tidak mendapatkan Alamat Image", Toast.LENGTH_SHORT);
//        }
//    }

    public String imagePath;
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();

            if (data.toString().contains("content:")) {
                imagePath = getRealPathFromURI(selectedImage);
            } else if (data.toString().contains("file:")) {
                imagePath = selectedImage.getPath();
            } else {
                imagePath = null;
            }
            Log.d("zzzgaleri",imagePath);
            imageList.add(imagePath);
            //mAdapter = new MyAdapter(imageList);
            mAdapter = new ImageListAdapter(getApplicationContext(), imageList);

            recyclerView.setAdapter(mAdapter);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null,
                    null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void addItemsOnPSI() {
        PsiAdapter psiAdapter = new PsiAdapter(ConstantVar.listPSI, MdrFormActivity.this);
        mComboPSI.setAdapter(psiAdapter);
        //spinner_country.setSelection(adapter.getPosition(myItem));//Optional to set the selected item.

        mComboPSI.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("adaperor",parent.getSelectedItem().toString());

                ObjPSI psi = ConstantVar.listPSI.get(position);
                //String pesan = "Key: "+psi.getKey()+" Name: "+ psi.getValue();
                //Toast.makeText(getApplicationContext(),pesan,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    String pObjStaType = "";
    public void addItemsOnStationType() {

        StaTypeAdapter staAdapter = new StaTypeAdapter(ConstantVar.listStationType, MdrFormActivity.this);
        mComboStatype.setAdapter(staAdapter);
        //spinner_country.setSelection(adapter.getPosition(myItem));//Optional to set the selected item.

        mComboStatype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("adaperor",parent.getSelectedItem().toString());

                ObjStaType def = ConstantVar.listStationType.get(position);
                pObjStaType = def.getKode();
//                String pesan = "Key: "+def.getKey()+" Name: "+ def.getValue();
//                Toast.makeText(getApplicationContext(),pesan,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    String pObjDefLimit = "";
    public void addItemsOnDefectLimit() {

        DefectLimitAdapter defAdapter = new DefectLimitAdapter(ConstantVar.listDefect, MdrFormActivity.this);
        mComboDefect.setAdapter(defAdapter);
        mComboDefect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("adaperor",parent.getSelectedItem().toString());
                ObjDefectLimit def = ConstantVar.listDefect.get(position);
                pObjDefLimit = def.getKode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void sendTrans(){
        showProgress(true);
        try {
            String token = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
            String ernam = mPrefSession.getString(ConstantVar.PREF_ACCOUNT,null);
            String orig_order = mPrefSession.getString(ConstantVar.PREF_AUFNR,null);
            String floc = mPrefSession.getString(ConstantVar.PREF_FLOC,null);
            String mworkcenter = mPrefSession.getString(ConstantVar.PREF_M_WORK_CENTER,null);
            String workcenter = mPrefSession.getString(ConstantVar.PREF_WORK_CENTER,null);
            String pm_ps = mPrefSession.getString(ConstantVar.PREF_PMPS,null);
            String mat = mPrefSession.getString(ConstantVar.PREF_MAT,null);
            String revnr = mPrefSession.getString(ConstantVar.PREF_REVNR,null);

            String mdr_desc = mTxtDefectTitle.getText().toString();
            String op_text = mTxtOprnRemake.getText().toString();
            String mdr_grup = popMdr.getM_mdrcode();
            String mdr_cod = popMdr.getM_subcode();

            String smr_sta_type = pObjStaType;
            String smr_sta_from = mTxtStaFrom.getText().toString();
            String smr_sta_to = mTxtStaTo.getText().toString();
            String smr_str_from = mTxtStringFrom.getText().toString();
            String smr_str_to = mTxtStringTo.getText().toString();
            String smr_wl_from = mTxtWLFrom.getText().toString();
            String smr_wl_to = mTxtWLTo.getText().toString();
            String smr_bl_from = mTxtBLFrom.getText().toString();
            String smr_bl_to = mTxtBLTo.getText().toString();
            String smr_zone = mTxtZone.getText().toString();
            String smr_clk_pst = mTxtClock.getText().toString();
            String smr_defect_limit = pObjDefLimit;

            String smr_objgrp = popObjpart.getM_mdrcode();
            String smr_objcod = popObjpart.getM_subcode();
            String smr_urgrp = popCause.getM_mdrcode();
            String smr_urcod = popCause.getM_subcode();
            String smr_urtxt = mTxtCauseText.getText().toString();
            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.setMdrTrans(
                    token,
                    ernam,
                    orig_order,
                    floc,
                    mworkcenter,
                    workcenter,
                    pm_ps,
                    mat,
                    revnr,

                    mdr_desc,
                    op_text,
                    mdr_grup,
                    mdr_cod,

                    smr_sta_type,
                    smr_sta_from,
                    smr_sta_to,
                    smr_str_from,
                    smr_str_to,
                    smr_wl_from,
                    smr_wl_to,
                    smr_bl_from,
                    smr_bl_to,
                    smr_zone,
                    smr_clk_pst,
                    smr_defect_limit,
                    smr_objgrp,
                    smr_objcod,
                    smr_urgrp,
                    smr_urcod,
                    smr_urtxt
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){

                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
                            Object s = (Object) response.body().getResultdata();
                            LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                            map = (LinkedTreeMap<String, String>) s;
                            String psnSukses = "MDR Order : "+map.get("AUFNR_NEW")
                                    +"\nNotification : "+map.get("QMNUM");
                            String idtrans=  map.get("TRANS_ID");
                            String mdr_order=map.get("AUFNR_NEW");

                            //Action Upload image
                            if(recyclerView.getChildCount()>0){
                                uploadImage(idtrans,mdr_order);
                            }

                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            PrettyDialog pDialog = new PrettyDialog(MdrFormActivity.this);
                            pDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                            pDialog.setTitle("Success")
                                    .setIcon(R.drawable.pdlg_icon_success,
                                            R.color.pdlg_color_green,
                                            new PrettyDialogCallback() {   // icon OnClick listener
                                                @Override
                                                public void onClick() {
                                                    pDialog.dismiss();
                                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                    Intent k = new Intent(getApplicationContext(), MainActivity.class);
                                                    k.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(k);
                                                    finish();
                                                }
                                            })
                                    .setMessage(psnSukses)
                                    .show();


                        }else{
                            String err = response.body().getMessage();
                            showProgress(false);
                            new PrettyDialog(MdrFormActivity.this)
                                    .setTitle("Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        Log.d("Retro",response.message());
                        showProgress(false);
                        new PrettyDialog(MdrFormActivity.this)
                                .setTitle("Failed")
                                .setMessage(response.message())
                                .show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    showProgress(false);
                    new PrettyDialog(MdrFormActivity.this)
                            .setTitle("Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });

        } catch(Exception e) {
            showProgress(false);
            new PrettyDialog(MdrFormActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Check Your Connection")
                    .show();
//            e.printStackTrace();
            showProgress(false);
        }
    }

    private void uploadImage(String idtrans, String mdrorder){
        //String pathx = "";
        //ListUploadImage dataImage = null;
        ObjUploadImage xdataImg;
        ArrayList<ObjUploadImage> listimg = new ArrayList<ObjUploadImage>();
        String xtoken = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
        String uname = mPrefSession.getString(ConstantVar.PREF_ACCOUNT,null);

        for (String imgpath : imageList){
            xdataImg = new ObjUploadImage();

            xdataImg.setIdtrans(idtrans);
            xdataImg.setToken(xtoken);
            xdataImg.setImgpath(imgpath);
            xdataImg.setOrderno(mdrorder);
            //Long timestamp = System.currentTimeMillis();
            Long timestamp = TimeUnit.MILLISECONDS.toMicros(System.nanoTime());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ndate = sdf.format(new Date());

            String nmfile = uname+"_"+ndate+System.nanoTime()+".jpg";
            xdataImg.setNama(nmfile);
            xdataImg.setUrlimage("http://dev.gmf-aeroasia.com/app_mdr/public/index.php/api/v1/mdr/get-image/"+nmfile);

            listimg.add(xdataImg);
        }

        Intent inten = new Intent(MdrFormActivity.this, BgJobImageUploadService.class);
        //inten.putStringArrayListExtra("datane", datlist);
        inten.putExtra("TagParam",listimg);
        startService(inten);

    }

    private void showProgress(final boolean show) {
        if(show){
            mLoadingFrm.setVisibility(View.VISIBLE);
        }else{
            mLoadingFrm.setVisibility(View.GONE);
        }
    }
}
