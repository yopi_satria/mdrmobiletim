package com.gmf.mdrcreation;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Wave;
import com.gmf.mdrcreation.model.ResponseApi;
import com.gmf.mdrcreation.network.RetrofitClientInstance;
import com.gmf.mdrcreation.service.ApiService;
import com.gmf.mdrcreation.util.ConstantVar;
import com.google.gson.internal.LinkedTreeMap;
import com.google.zxing.integration.android.IntentIntegrator;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MdrScanActivity extends AppCompatActivity {
    //static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    Button mbtnScan;
    Button mbtnEnter;
    final Activity activity = this;
    EditText mtxtResult;
    SharedPreferences sharedPreferences;

    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mdr_scan);

        mbtnScan = (Button) findViewById(R.id.btnscan);
        mbtnEnter = (Button) findViewById(R.id.btnenter);
        mtxtResult = (EditText) findViewById(R.id.textscan);
        sharedPreferences = getSharedPreferences(ConstantVar.PREF_SESSION, 0);

        //setup loading
        mLoadingFrm = (RelativeLayout) findViewById(R.id.loadingfrm);
        Wave loadingstyle = new Wave();
        loadingstyle.setColor(Color.WHITE);
        mLoadingBar = (ProgressBar) findViewById(R.id.loadingbar);
        mLoadingBar.setIndeterminateDrawable(loadingstyle);


        mbtnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentScan = new IntentIntegrator(activity);
                intentScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                intentScan.setBeepEnabled(true);
                intentScan.setPrompt("Scan Originating Number");
                intentScan.setOrientationLocked(false);
                intentScan.setCameraId(0);
                intentScan.setBarcodeImageEnabled(false);
                intentScan.initiateScan();
            }
        });



        mbtnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mProgressView.setVisibility(View.VISIBLE);
                try {
                    cekOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //cekOrderTest();
            }
        });
        //showProgress(true);
    }

//    private void cekOrderTest(){
//        try {
//            showProgress(true);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    showProgress(false);
//                    if(mtxtResult.length()>0) {
//                        Intent k = new Intent(getApplicationContext(), MdrFormActivity.class);
//                        startActivity(k);
//                    }  else{
//                        PrettyDialog pDialog = new PrettyDialog(MdrScanActivity.this);
//                        pDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//                                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
//                        pDialog.setTitle("Success")
//                                .setIcon(R.drawable.pdlg_icon_success,
//                                        R.color.pdlg_color_green,
//                                        new PrettyDialogCallback() {   // icon OnClick listener
//                                            @Override
//                                            public void onClick() {
//                                                pDialog.dismiss();
//                                                Intent k = new Intent(getApplicationContext(), MdrFormActivity.class);
//                                                startActivity(k);
//                                            }
//                                        })
//                                .setMessage("Please Input MDR")
//                                .show();
////
//                    }
//                }
//            },2000);
//        } catch(Exception e) {
//            //e.printStackTrace();
//            new PrettyDialog(MdrScanActivity.this)
//                    .setTitle("Failed")
//                    .setMessage("Please Check Your Connection")
//                    .show();
//        }
//    }

    private void showProgress(final boolean show) {
        if(show){
            mLoadingFrm.setVisibility(View.VISIBLE);
        }else{
            mLoadingFrm.setVisibility(View.GONE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //mtxtResult = (EditText) findViewById(R.id.textscan);
        //IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        //if (scanResult != null) {
        if (resultCode == RESULT_OK) {
            String ordernumb =intent.getStringExtra("SCAN_RESULT");
            //ambil 9 angka dari angka ke 5
            String strOut = ordernumb.substring(4,13);
            mtxtResult.setText(strOut);

        }else if (resultCode == RESULT_CANCELED) {
            // Handle cancels
            Toast toast = Toast.makeText(this, "Scan was Cancelled!", Toast.LENGTH_LONG);
            //toast.setGravity(Gravity.TOP, 25, 400);
            toast.show();

        }

    }

    private void cekOrder() throws Exception {
        showProgress(true);
        if(TextUtils.isEmpty(mtxtResult.getText())){
            showProgress(false);
            new PrettyDialog(MdrScanActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please input Originating Number")
                    .show();
        }else {

            String token = sharedPreferences.getString(ConstantVar.PREF_TOKEN_KEY,null);

            String orderno = mtxtResult.getText().toString().trim();
            //int intVal = Integer.parseInt(mtxtResult.getText().toString());

            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getCekOrder(
                    token,
                    orderno
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
                            String detailOrder = "";
                            Object s = (Object) response.body().getResultdata();
                            System.out.println(s.toString());
                            LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                            if(!s.toString().equals("[]")) {
                                map = (LinkedTreeMap<String, String>) s;

                                detailOrder = "Order "+orderno+ " Valid\n";
                                detailOrder += "Title : "+ map.get("ORDER_TITLE")+"\n";
                                detailOrder += "A/C Registration : "+ map.get("FLOC")+"\n";
                                detailOrder += "Revision : "+ map.get("REVISION")+"\n";
                                System.out.println(map.toString());
                                System.out.println("oke jek" +map.get("m_work_center"));

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(ConstantVar.PREF_AUFNR, orderno);
                                editor.putString(ConstantVar.PREF_FLOC, map.get("FLOC"));
                                editor.putString(ConstantVar.PREF_WORK_CENTER, map.get("WORK_CENTER"));
                                editor.putString(ConstantVar.PREF_M_WORK_CENTER, map.get("M_WORK_CENTER"));
                                editor.putString(ConstantVar.PREF_ORDER_TITLE, detailOrder);
                                editor.putString(ConstantVar.PREF_MAT, map.get("MAT"));
                                editor.putString(ConstantVar.PREF_PMPS, map.get("PM_PS"));
                                editor.putString(ConstantVar.PREF_REVNR, map.get("REVISION"));
                                editor.commit();
                            }

                            showProgress(false);

                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            PrettyDialog pDialog = new PrettyDialog(MdrScanActivity.this);
                            pDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                            pDialog.setTitle("Success")
                                    .setIcon(R.drawable.pdlg_icon_success,
                                            R.color.pdlg_color_green,
                                            new PrettyDialogCallback() {   // icon OnClick listener
                                                @Override
                                                public void onClick() {
                                                    pDialog.dismiss();
                                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                    Intent k = new Intent(getApplicationContext(), MdrFormActivity.class);
                                                    startActivity(k);
                                                }
                                            })
                                    .setMessage(detailOrder)
                                    .show();
//
                        }else{
                            showProgress(false);
                            String err = response.body().getMessage();
                            //Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();
                            new PrettyDialog(MdrScanActivity.this)
                                    .setTitle("Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        //Log.d("Retro",response.message());
                        showProgress(false);
                        new PrettyDialog(MdrScanActivity.this)
                                .setTitle("Failed")
                                .setMessage(response.message().toString())
                                .show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    //Log.d("Retro",t.toString());
                    showProgress(false);
                    new PrettyDialog(MdrScanActivity.this)
                            .setTitle("Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });
        }

    }
}
