package com.gmf.mdrcreation;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gmf.mdrcreation.adapter.ImageListSummaryAdapter;
import com.gmf.mdrcreation.model.ObjMDRList;
import com.gmf.mdrcreation.model.ResponseApi;
import com.gmf.mdrcreation.network.RetrofitClientInstance;
import com.gmf.mdrcreation.service.ApiService;
import com.gmf.mdrcreation.util.ConstantVar;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SummaryDetailActivity extends AppCompatActivity {
    TextView mTplnr,mAircraft, mRevision, mMDRno, mMDRDesc, mRaised,mTglCreate;
    private RecyclerView mRvImage;
    private RecyclerView.Adapter mAdapter;
    List<String> imageList = new ArrayList<>();
    SharedPreferences sharedPreferences;

    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_detail);
        sharedPreferences = getSharedPreferences(ConstantVar.PREF_SESSION, 0);

        mTplnr = findViewById(R.id.txtplnr);
        mAircraft = findViewById(R.id.txtaircraft);
        mRevision = findViewById(R.id.txtrevnumber);
        mMDRno = findViewById(R.id.txtmdrno);
        mMDRDesc = findViewById(R.id.txtmdrdesc);
        mRaised = findViewById(R.id.txternam);
        mTglCreate = findViewById(R.id.txtcreated);


        ArrayList<ObjMDRList> myObj = (ArrayList<ObjMDRList>) getIntent().getSerializableExtra("dataMDR");

        mTplnr.setText(myObj.get(0).getTplnr());
        mAircraft.setText(myObj.get(0).getTplnr());
        mRevision.setText(myObj.get(0).getRevnr());
        mMDRno.setText(myObj.get(0).getMdrorder());
        mMDRDesc.setText(myObj.get(0).getMdrdesc());
        mRaised.setText("Raised by : "+myObj.get(0).getErnam());
        mTglCreate.setText(myObj.get(0).getMdrdate());

        mRvImage = findViewById(R.id.rvimage);
        mRvImage.setLayoutManager(new GridLayoutManager(this, 2));
        mRvImage.setHasFixedSize(true);

//        imageList.add("http://10.0.2.2/api-mdr-mobile/assets/image/g324242_2018010234.jpg");
//        imageList.add("http://10.0.2.2/api-mdr-mobile/assets/image/rano.dts_20180901223829512806704.jpg");
//        imageList.add("http://10.0.2.2/api-mdr-mobile/assets/image/rano.dts_20180901226131243114785.jpg");
//        //mAdapter = new MyAdapter(imageList);
//        mAdapter = new ImageListSummaryAdapter(getApplicationContext(), imageList);
//        mRvImage.setAdapter(mAdapter);

        loadImage(myObj.get(0).getMdrorder());

//        for (ListOrderMDR aa :  myObj){
//                System.out.println( aa.getMdrorder());
//                System.out.println( aa.getTplnr());
//                System.out.println( aa.getRevnr());
////            ListOrderMDR order = new ListOrderMDR();
////            order.setOrderno(aa.getMdrorder());
////            order.setDescription(aa.getMdrdesc());
////            order.setOrderorig(aa.getOrigorder());
////            order.setTglcreate(aa.getMdrdate());
////            aircraftNo =  aa.getTplnr();
////            revNo = aa.getRevnr();
////            orderArrayList.add(order);
//        }
    }

    private void loadImage(String mdrOrder)  {
        //showProgress(true);
        try {
            String token = sharedPreferences.getString(ConstantVar.PREF_TOKEN_KEY, null);
            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getSummaryDetail(
                    token,
                    mdrOrder
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getCodestatus().equalsIgnoreCase("S")) {
                            Object s = (Object) response.body().getResultdata();
                            LinkedTreeMap<String, Object> map = new LinkedTreeMap<String, Object>();
                            ArrayList<Object> mapImg = new ArrayList<Object>();
                            if (!s.toString().equals("[]")) {
                                map = (LinkedTreeMap<String, Object>) s;
                                mapImg = (ArrayList<Object>) map.get("image");

                                if (mapImg != null){
                                    for (int i = 0; i < mapImg.size(); i++) {
                                        String urlpath = mapImg.get(i).toString();
                                        imageList.add(urlpath);
                                        System.out.println(urlpath);
                                    }
                                }
                                mAdapter = new ImageListSummaryAdapter(getApplicationContext(), imageList);
                                mRvImage.setAdapter(mAdapter);
                            }
                        } else {
//                            showProgress(false);
//                            String err = response.body().getMessage();
//                            //Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();
//                            new PrettyDialog(SummaryDetailActivity.this)
//                                    .setTitle("Failed")
//                                    .setMessage(err)
//                                    .show();
                        }
                    } else {
                        Log.d("Retro",response.message());
//                        showProgress(false);
//                        new PrettyDialog(SummaryDetailActivity.this)
//                                .setTitle("Failed")
//                                .setMessage(response.message().toString())
//                                .show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    Log.d("Retro",t.toString());
//                    showProgress(false);
//                    new PrettyDialog(SummaryDetailActivity.this)
//                            .setTitle("Failed")
//                            .setMessage("Connection Timeout")
//                            .show();
                }
            });

        }catch (Exception e){
            Log.d("Retro",e.getMessage());
        }
    }
}
