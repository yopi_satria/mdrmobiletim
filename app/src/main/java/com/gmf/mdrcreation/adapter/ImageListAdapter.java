package com.gmf.mdrcreation.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import com.gmf.mdrcreation.MdrFormActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.gmf.mdrcreation.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.MyViewHolder> {

    List<String> listData;
    Context mContext;

    public List<String> getListData() {
        return listData;
    }

    public ImageListAdapter(Context context, List<String> myDataset) {
        this.listData = myDataset;
        //this.mContext = context;
    }

    @NonNull
    @Override
    public ImageListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.list_imagepreview_backup, parent, false);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_mdr_image, parent, false);
        mContext = parent.getContext();
        return new ImageListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final String imagepath = listData.get(position);
        holder.name.setText(getfilename(imagepath));
        Picasso.with(mContext).load("file://"+imagepath)
                .config(Bitmap.Config.RGB_565).fit()
                .centerCrop().into(holder.image);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listData.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
                //View mView = getLayoutInflater().inflate(R.layout.dialog_image_preview, null);

//                View mView = inflater.inflate(R.layout.dialog_image_preview, null);
//                PhotoView photoView = mView.findViewById(R.id.imageView);

                v = inflater.inflate(R.layout.dialog_image_preview, null);
                PhotoView photoView = v.findViewById(R.id.imageView);
                photoView.setImageURI(Uri.parse(imagepath));
                mBuilder.setView(v);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public String data;
        ImageView image;
        TextView name;
        ImageView delete;
//        Button delete;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.vimage);
            name = view.findViewById(R.id.txtfilename);
            delete = view.findViewById(R.id.btnimagedel);
        }
    }

    public String getfilename(String s){
        String[] parts = s.split("/");
        return parts[parts.length-1];
    }
}
