package com.gmf.mdrcreation.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.gmf.mdrcreation.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ImageListSummaryAdapter extends RecyclerView.Adapter<ImageListSummaryAdapter.MyViewHolder> {
    List<String> listData;
    Context mContext;

    public List<String> getListData() {
        return listData;
    }

    public ImageListSummaryAdapter(Context context, List<String> myDataset) {
        this.listData = myDataset;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ImageListSummaryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_mdr_image, parent, false);
        mContext = parent.getContext();
        return new ImageListSummaryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageListSummaryAdapter.MyViewHolder holder, int position) {
        final String imagepath = listData.get(position);
        holder.name.setText(getfilename(imagepath));
        //Picasso.with(mContext).load("file://"+imagepath)
//        Picasso.with(mContext).load(imagepath)
////                .config(Bitmap.Config.RGB_565).fit()
////                .centerCrop()
//                .into(holder.image);
        //String urlimg = "http://localhost/api-mdr-mobile/assets/image/g324242_2018010234.jpg";
        Picasso.with(mContext).load(imagepath)
                .into(holder.image);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listData.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
                //View mView = getLayoutInflater().inflate(R.layout.dialog_image_preview, null);

//                View mView = inflater.inflate(R.layout.dialog_image_preview, null);
//                PhotoView photoView = mView.findViewById(R.id.imageView);

                //to image source
                Bitmap imgSource = getBitmapFromURL(imagepath);

                v = inflater.inflate(R.layout.dialog_image_preview, null);
                PhotoView photoView = v.findViewById(R.id.imageView);
                photoView.setImageBitmap(imgSource);
                //photoView.setImageURI(Uri.parse(imagepath));
                mBuilder.setView(v);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setInstanceFollowRedirects(true);
            //connection.setDoInput(true);
            //connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace(); return null;
        }
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public String data;
        ImageView image;
        TextView name;
        ImageView delete;
//        Button delete;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.vimage);
            name = view.findViewById(R.id.txtfilename);
            delete = view.findViewById(R.id.btnimagedel);
            delete.setVisibility(View.GONE);
        }
    }

    public String getfilename(String s){
        String[] parts = s.split("/");
        return parts[parts.length-1];
    }
}
