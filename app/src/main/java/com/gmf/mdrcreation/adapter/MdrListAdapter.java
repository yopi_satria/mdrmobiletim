package com.gmf.mdrcreation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmf.mdrcreation.R;
import com.gmf.mdrcreation.model.ListOrderMDR;
import com.gmf.mdrcreation.model.ObjMDRList;

import java.util.ArrayList;

public class MdrListAdapter extends BaseAdapter{

    ArrayList<ObjMDRList> orderList;
    Context mContext;

    public MdrListAdapter(Context context, ArrayList<ObjMDRList> orderArrayList) {
        this.mContext = context;
        this.orderList = orderArrayList;
    }

    @Override
    public int getCount() {
        return this.orderList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjMDRList order = orderList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_order,null);

        TextView orderno = (TextView) convertView.findViewById(R.id.text1);
        TextView desc = (TextView) convertView.findViewById(R.id.text2);
        TextView orderorig = (TextView) convertView.findViewById(R.id.text3);
        TextView tglcreate = (TextView) convertView.findViewById(R.id.text4);

        orderno.setText(order.getMdrorder());
        desc.setText(order.getMdrdesc());
        orderorig.setText(order.getOrigorder());
        tglcreate.setText(order.getMdrdate());

        return convertView;
    }
}
