package com.gmf.mdrcreation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmf.mdrcreation.R;
import com.gmf.mdrcreation.model.ObjPSI;

import java.util.ArrayList;

public class PsiAdapter extends BaseAdapter {
    ArrayList<ObjPSI> psiList;
    Context mContext;

    public PsiAdapter(ArrayList<ObjPSI> psiList, Context mContext) {
        this.psiList = psiList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return this.psiList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjPSI objPSI  = psiList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_combobox,null);
        TextView kode = (TextView) convertView.findViewById(R.id.txkode);
        TextView val = (TextView) convertView.findViewById(R.id.txvalue);

        kode.setText(objPSI.getKey());
        val.setText(objPSI.getValue());

        return convertView;
    }
}
