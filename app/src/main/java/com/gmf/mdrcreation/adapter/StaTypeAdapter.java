package com.gmf.mdrcreation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmf.mdrcreation.R;
import com.gmf.mdrcreation.model.ObjStaType;

import java.util.ArrayList;

public class StaTypeAdapter extends BaseAdapter {
    ArrayList<ObjStaType> staList;
    Context mContext;

    public StaTypeAdapter(ArrayList<ObjStaType> staList, Context mContext) {
        this.staList = staList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return this.staList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjStaType objSta  = staList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_combobox,null);
        TextView kode = (TextView) convertView.findViewById(R.id.txkode);
        TextView val = (TextView) convertView.findViewById(R.id.txvalue);

        kode.setText(objSta.getKode());
        val.setText(objSta.getName());

        return convertView;
    }
}
