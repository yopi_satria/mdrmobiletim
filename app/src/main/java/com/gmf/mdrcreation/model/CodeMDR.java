package com.gmf.mdrcreation.model;

public class CodeMDR {
    String keymdr, valuemdr;

    public CodeMDR(String keymdr, String valuemdr) {
        this.keymdr = keymdr;
        this.valuemdr = valuemdr;
    }

    public String getKeymdr() {
        return keymdr;
    }

    public void setKeymdr(String keymdr) {
        this.keymdr = keymdr;
    }

    public String getValuemdr() {
        return valuemdr;
    }

    public void setValuemdr(String valuemdr) {
        this.valuemdr = valuemdr;
    }
}
