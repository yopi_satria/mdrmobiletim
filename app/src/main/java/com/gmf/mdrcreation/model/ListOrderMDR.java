package com.gmf.mdrcreation.model;

import java.io.Serializable;

public class ListOrderMDR implements Serializable {
    String mdrorder,description,orderorig, tglcreate,tplnr,revnr, ernam;


    public String getMdrorder() {
        return mdrorder;
    }

    public void setMdrorder(String mdrorder) {
        this.mdrorder = mdrorder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrderorig() {
        return orderorig;
    }

    public void setOrderorig(String orderorig) {
        this.orderorig = orderorig;
    }

    public String getTglcreate() {
        return tglcreate;
    }

    public void setTglcreate(String tglcreate) {
        this.tglcreate = tglcreate;
    }

    public String getTplnr() {
        return tplnr;
    }

    public void setTplnr(String tplnr) {
        this.tplnr = tplnr;
    }

    public String getRevnr() {
        return revnr;
    }

    public void setRevnr(String revnr) {
        this.revnr = revnr;
    }

    public String getErnam() {
        return ernam;
    }

    public void setErnam(String ernam) {
        this.ernam = ernam;
    }
}
