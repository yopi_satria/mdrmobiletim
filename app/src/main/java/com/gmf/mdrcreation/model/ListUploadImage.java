package com.gmf.mdrcreation.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListUploadImage implements Serializable{
    ArrayList<ObjUploadImage> listImage;

    public ArrayList<ObjUploadImage> getListImage() {
        return listImage;
    }

    public void setListImage(ArrayList<ObjUploadImage> listImage) {
        this.listImage = listImage;
    }
}
