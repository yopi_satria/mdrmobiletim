package com.gmf.mdrcreation.model;

public class ObjDefectLimit {
    String kode,name;

    public ObjDefectLimit(String kode, String name) {
        this.kode = kode;
        this.name = name;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ObjPSI){
            ObjDefectLimit c = (ObjDefectLimit)obj;
            if(c.getKode().equals(kode) && c.getName().equals(name) ) return true;
        }
        return false;
    }
}
