package com.gmf.mdrcreation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ObjMDRList implements Serializable {
    String mdrorder, mdrdesc, origorder, mdrdate, tplnr,  revnr, ernam;

    public String getMdrorder() {
        return mdrorder;
    }

    public void setMdrorder(String mdrorder) {
        this.mdrorder = mdrorder;
    }

    public String getMdrdesc() {
        return mdrdesc;
    }

    public void setMdrdesc(String mdrdesc) {
        this.mdrdesc = mdrdesc;
    }

    public String getOrigorder() {
        return origorder;
    }

    public void setOrigorder(String origorder) {
        this.origorder = origorder;
    }

    public String getMdrdate() {
        return mdrdate;
    }

    public void setMdrdate(String mdrdate) {
        this.mdrdate = mdrdate;
    }

    public String getTplnr() {
        return tplnr;
    }

    public void setTplnr(String tplnr) {
        this.tplnr = tplnr;
    }

    public String getRevnr() {
        return revnr;
    }

    public void setRevnr(String revnr) {
        this.revnr = revnr;
    }

    public String getErnam() {
        return ernam;
    }

    public void setErnam(String ernam) {
        this.ernam = ernam;
    }
}
