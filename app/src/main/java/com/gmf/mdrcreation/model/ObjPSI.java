package com.gmf.mdrcreation.model;

public class ObjPSI {
    String key,value;

    public ObjPSI(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ObjPSI){
            ObjPSI c = (ObjPSI)obj;
            if(c.getKey().equals(key) && c.getValue().equals(value) ) return true;
        }
        return false;
    }
}
