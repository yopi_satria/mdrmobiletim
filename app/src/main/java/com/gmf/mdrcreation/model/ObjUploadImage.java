package com.gmf.mdrcreation.model;

import java.io.Serializable;

public class ObjUploadImage implements Serializable{
    String token, nama, urlimage, imgpath, idtrans, orderno;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUrlimage() {
        return urlimage;
    }

    public void setUrlimage(String urlimage) {
        this.urlimage = urlimage;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public String getIdtrans() { return idtrans; }

    public void setIdtrans(String idtrans) { this.idtrans = idtrans; }

    public String getOrderno() {    return orderno;  }

    public void setOrderno(String orderno) {  this.orderno = orderno;    }
}
