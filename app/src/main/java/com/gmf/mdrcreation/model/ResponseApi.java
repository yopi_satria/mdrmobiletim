package com.gmf.mdrcreation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseApi implements Serializable {
    @SerializedName("codestatus")
    private String codestatus;
    @SerializedName("message")
    private String message;
    @SerializedName("resultdata")
    private Object resultdata;

    public ResponseApi(String codestatus, String message, Object resultdata) {
        this.codestatus = codestatus;
        this.message = message;
        this.resultdata = resultdata;
    }

    public String getCodestatus() {
        return codestatus;
    }

    public void setCodestatus(String codestatus) {
        this.codestatus = codestatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResultdata() {
        return resultdata;
    }

    public void setResultdata(Object resultdata) {
        this.resultdata = resultdata;
    }
}
