package com.gmf.mdrcreation.model;

public class SubCodeMDR {
    String keymdr;
    String keysubcod;
    String valuesubcod;

    public SubCodeMDR(String keymdr, String keysubcod, String valuesubcod) {
        this.keymdr = keymdr;
        this.keysubcod = keysubcod;
        this.valuesubcod = valuesubcod;
    }

    public String getKeymdr() {
        return keymdr;
    }

    public void setKeymdr(String keymdr) {
        this.keymdr = keymdr;
    }

    public String getKeysubcod() {
        return keysubcod;
    }

    public void setKeysubcod(String keysubcod) {
        this.keysubcod = keysubcod;
    }

    public String getValuesubcod() {
        return valuesubcod;
    }

    public void setValuesubcod(String valuesubcod) {
        this.valuesubcod = valuesubcod;
    }
}
