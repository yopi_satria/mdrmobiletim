package com.gmf.mdrcreation.network;


import android.util.Log;

import com.gmf.mdrcreation.model.ResponseApi;
import com.gmf.mdrcreation.service.ApiService;
import com.google.gson.internal.LinkedTreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestApi {

    public static void main(String[] args) {
        //usertest();
        //userlogin();
        cekorder();
    }

    public static void usertest(){
        try {
            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getLoginTest();
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        //Log.d("Retro",response.body().getMessage().toString());
                        Object s = response.body().getResultdata();
                        LinkedTreeMap<String,String> map = (LinkedTreeMap<String,String>) s;

                        System.out.println(map.get("description"));
                    }else{
                        //Log.d("Retro",response.message().toString());
                        System.out.println(response.body().getResultdata().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    Log.d("Retro",t.getMessage());
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void userlogin(){
        try {
            //String username = "ysd";
            //String password = "oke";
            String username = "rano.dts";
            String password = "libranofauzan";
            String manufacture = "Xiaomi";//Build.MANUFACTURER; //merek
            String model = "MI Note 3 Pro";//Build.MODEL;  //Tipe Hape
            String version = "7.0.1";//Build.VERSION.RELEASE; //Android
            //Jenis Android
            String sdk = "Nougat";//Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getLoginUser(username, password,
                    manufacture,
                    model,
                    version,
                    sdk);
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        //Log.d("Retro",response.body().getMessage().toString());
                        Object s = (Object) response.body().getResultdata();
                        LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                        if(!s.toString().equals("[]")) {
                            map = (LinkedTreeMap<String, String>) s;
                            System.out.println(map.get("displayname"));
                            System.out.println(map.get("description"));
                        }
                        else
                            System.out.println(response.body().getResultdata().toString());

                    }else{
                        //Log.d("Retro",response.message().toString());
                        System.out.println("ERROR : " + response.message().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    Log.d("Retro",t.getMessage());
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void cekorder(){
        try {
            //String username = "ysd";
            //String password = "oke";
            String token = "72b6d7ad644c8a5cc2fb02d4733513c9";
            String order = "800435652";
            //Log.d("rrrerr","72b6d7ad644c8a5cc2fb02d4733513c9");
            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getCekOrder(token, order);
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        //Log.d("Retro",response.body().getMessage().toString());
                        Object s = (Object) response.body().getResultdata();
                        LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                        if(!s.toString().equals("[]")) {
                            map = (LinkedTreeMap<String, String>) s;

                            System.out.println(map.toString());
                            System.out.println("oke jek" +map.get("m_work_center"));
                        }
                        else
                            System.out.println(response.body().getResultdata().toString());

                    }else{
                        //Log.d("Retro",response.message().toString());
                        System.out.println("ERROR : " + response.message().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    //Log.d("Retro",t.getMessage());
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
