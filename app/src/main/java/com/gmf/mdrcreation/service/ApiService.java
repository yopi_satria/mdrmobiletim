package com.gmf.mdrcreation.service;

import com.gmf.mdrcreation.model.ResponseApi;

import okhttp3.Connection;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;


public interface ApiService {
    //Test
    @GET("login-test2")
    Call<ResponseApi> getLoginTest();

    //Login Application
//    @FormUrlEncoded
//    @POST("login-user")
//    Call<ResponseApi> getLoginUser(@Field("username") String username,
//                                   @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/login")
    Call<ResponseApi> getLoginUser(@Field("username") String username,
                                   @Field("password") String password,
                                   @Field("manufacture") String manufacture,
                                   @Field("model") String model,
                                   @Field("version") String version,
                                   @Field("sdk") String sdk
                                   );


    //Create Transaction
//    @FormUrlEncoded
//    @POST("cek-order")
//    Call<ResponseApi> getCekOrder(@Field("orderno") String orderno);

    @FormUrlEncoded
    @POST("mdr/get-order")
    Call<ResponseApi> getCekOrder( @Field("token") String token,
                                   @Field("order") String order
                                 )  throws Exception;


//    @Field("smr_psessi") String smr_psessi,
    @FormUrlEncoded
    @POST("mdr/create-mdr")
    Call<ResponseApi> setMdrTrans(
            @Field("token") String token,
            @Field("ernam") String ernam,
            @Field("orig_order") String orig_order,
            @Field("floc") String floc,
            @Field("mworkcenter") String mworkcenter,
            @Field("workcenter") String workcenter,
            @Field("pm_ps") String pm_ps,
            @Field("mat") String mat,
            @Field("revnr") String revnr,

            @Field("mdr_desc") String mdr_desc,
            @Field("op_text") String op_text,
            @Field("mdr_grup") String mdr_grup,
            @Field("mdr_cod") String mdr_cod,

            @Field("smr_sta_type") String smr_sta_type,
            @Field("smr_sta_from") String smr_sta_from,
            @Field("smr_sta_to") String smr_sta_to,
            @Field("smr_str_from") String smr_str_from,
            @Field("smr_str_to") String smr_str_to,
            @Field("smr_wl_from") String smr_wl_from,
            @Field("smr_wl_to") String smr_wl_to,
            @Field("smr_bl_from") String smr_bl_from,
            @Field("smr_bl_to") String smr_bl_to,
            @Field("smr_zone") String smr_zone,
            @Field("smr_clk_pst") String smr_clk_pst,
            @Field("smr_defect_limit") String smr_defect_limit,
            @Field("smr_objgrp") String smr_objgrp,
            @Field("smr_objcod") String smr_objcod,
            @Field("smr_urgrp") String smr_urgrp,
            @Field("smr_urcod") String smr_urcod,
            @Field("smr_urtxt") String smr_urtxt
    );


    @POST("mdr/upload-image")
    Call<ResponseApi> setUploadImage( @Header("Content-Type") String contentType,
                                      @Body MultipartBody body
                                    );

    @FormUrlEncoded
    @GET("mdr/get-image/{name}")
    Call<Object> getImage(@Path("name") String name);

    //Summary MDR
    @FormUrlEncoded
    @POST("master/summary-list")
    Call<ResponseApi> getSummaryList(
            @Field("token") String token,
            @Field("revnr") String revnr,
            @Field("airreg") String airreg,
            @Field("datefr") String datefr,
            @Field("dateto") String dateto,
            @Field("origin") String origin,
            @Field("defect") String defect
    );

    @FormUrlEncoded
    @POST("master/summary-detail")
    Call<ResponseApi> getSummaryDetail(
            @Field("token") String token,
            @Field("mdrno") String mdrno
    );

}
