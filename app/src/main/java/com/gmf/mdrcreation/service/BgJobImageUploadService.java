package com.gmf.mdrcreation.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.gmf.mdrcreation.MdrFormActivity;
import com.gmf.mdrcreation.R;
import com.gmf.mdrcreation.model.ObjUploadImage;
import com.gmf.mdrcreation.model.ResponseApi;
import com.gmf.mdrcreation.network.RetrofitClientInstance;
import com.gmf.mdrcreation.util.UtilityApp;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BgJobImageUploadService extends Service {
    ObjUploadImage objImage;
    public BgJobImageUploadService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        //Toast.makeText(this, "Oncreate Service Jalan", Toast.LENGTH_LONG).show();

        Intent notificationIntent = new Intent(this, MdrFormActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.icon_app)
                .setContentTitle("MDR Creation")
                .setContentText("Upload Image Process...")
                .setContentIntent(pendingIntent).build();

        startForeground(1, notification);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "StartCommand Jalan", Toast.LENGTH_LONG).show();

        ArrayList<ObjUploadImage> myObj = (ArrayList<ObjUploadImage>) intent.getSerializableExtra("TagParam");
        //ListUploadImage myObj = (ListUploadImage) intent.getSerializableExtra("TagParam");
        int n=0;
        for (ObjUploadImage aa :  myObj){

            System.out.println( aa.getToken());
            System.out.println( aa.getImgpath());
            System.out.println( aa.getNama());

            uploadImageToServer(intent,
                    aa.getToken(),
                    aa.getImgpath(),
                    aa.getNama(),
                    aa.getIdtrans(),
                    aa.getOrderno()
            );

            n++;
        }

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("stoping...");
//                stopSelf();
//            }
//        },10000);
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        //Toast.makeText(this, "OnDestroy Jalan", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

    private void uploadImageToServer(Intent intent,String token, String pathx,
                                     String namafile, String idtrans, String mdr_order){
        try {

            Bitmap temp = BitmapFactory.decodeFile(pathx);
            if(UtilityApp.sizeOfImage(temp) > 3000000){
                temp = UtilityApp.scaleDown(temp, 1536, true);
            }
            File tempfile = UtilityApp.convertImageToFile(this, temp, "temp_"+namafile);
            File file;
            if (tempfile == null){
                file = new File(pathx);
            }else{
                file = tempfile;
            }

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody body = new MultipartBody.Builder()
                    .addFormDataPart("token", token)
                    .addFormDataPart("image", namafile, reqFile)
                    .addFormDataPart("nama", namafile)
                    .addFormDataPart("transaction_id", idtrans)
                    .addFormDataPart("mdr_order", mdr_order)
                    .build();
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);

            retrofit2.Call<ResponseApi> req = service.setUploadImage(
                    "multipart/form-data; boundary=" + body.boundary(),
                    body
            );
            req.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){

                        if(response.body().getCodestatus().equalsIgnoreCase("S")){

                            System.out.println("Sukses Upload Image");
                        }else{
                            System.out.println("Gagal Upload Image");
                        }
                    }else{
                        System.out.println("Failure Upload Image");
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                   System.out.println("Failure Upload Image");
                }
            });

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
