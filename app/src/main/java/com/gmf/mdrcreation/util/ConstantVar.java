package com.gmf.mdrcreation.util;

import com.gmf.mdrcreation.model.ObjDefectLimit;
import com.gmf.mdrcreation.model.ObjPSI;
import com.gmf.mdrcreation.model.ObjStaType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConstantVar {

    //Development Server
    //public static final String BASE_URL = "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/";
    //public static final String BASE_URL = "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/local/v1/";

    //Development Localhost
    //public static final String BASE_URL = "http://10.0.2.2/api-mdr-mobile/public/index.php/local/v1/";
    public static final String BASE_URL = "http://172.16.147.81/api-mdr-mobile/public/index.php/local/v1/";

    //Development Hard Test
    //public static final String BASE_URL = "http://10.0.2.2/api-mdr-mobile/public/index.php/hard/v1/";
    //public static final String BASE_URL = "http://192.168.42.179/api-mdr-mobile/public/index.php/hard/v1/";


    //SESSION
    public static final String PREF_SESSION = "SessionUser" ;
        public static final String PREF_NAME_KEY = "fullname" ;
        public static final String PREF_USERNAME_KEY = "username" ;
        public static final String PREF_USERDESC_KEY = "description" ;
        public static final String PREF_EMAIL_KEY = "email" ;
        public static final String PREF_TOKEN_KEY = "apitoken" ;
        public static final String PREF_ACCOUNT = "account";

    //Data
    public static final String PREF_DATAORDER = "DataOrder" ;
        public static final String PREF_AUFNR = "AUFNR" ;
        public static final String PREF_FLOC = "FLOC" ;
        public static final String PREF_WORK_CENTER = "WORK_CENTER" ;
        public static final String PREF_M_WORK_CENTER = "M_WORK_CENTER" ;
        public static final String PREF_ORDER_TITLE= "ORDER_TITLE" ;
        public static final String PREF_MAT= "MAT" ;
        public static final String PREF_PMPS= "PMPS" ;
        public static final String PREF_REVNR= "REVNR" ;


//    static final Map<String, String> mapOnPSI = new LinkedHashMap<String, String>() {{
//        put("Y","Default");
//        put("N","NO");
//        put("Y","YES");
//    }};
//    static final Map<String, String> mapDefect = new LinkedHashMap<String, String>() {{
//        put("W","Within Approved Limits");
//        put("O","Outside Approved Limits");
//        put("I","Under Investigation");
//    }};
//    static final Map<String, String> mapStationType = new LinkedHashMap<String, String>() {{
//        put("WBL","Wing Buttock Line");
//        put("STAB","Wing Buttock Line");
//        put("FIN","Wing Buttock Line");
//        put("IFS","Wing Buttock Line");
//        put("OFS","Wing Buttock Line");
//        put("ILE","Wing Buttock Line");
//        put("OLE","Outboard Leading Edge");
//    }};

    public static final String[] ZPM_AVI = {
            "AC	 Arcing-connector",
            "AS	 Arcing-splice",
            "AT	 Arcing-terminal",
            "AW	 Arcing-wire",
            "BD	 Binding",
            "BN	 Burn/scortch",
            "BO	 Bulp Burn Out",
            "BR	 Broken",
            "BT	 burst",
            "CB	 Connector, Contact, bent/pushed back",
            "CC	 Connector-contaminated",
            "CD	 Connector-damaged",
            "CO	 Corrosion, Connections, Grounds/bond",
            "DM	 Damage",
            "DR	 Dirty",
            "EC	 Overheating-evidence of connector",
            "ED	 Erosion damage",
            "GC	 Ground Circuit",
            "IB	 Indication Blank/No indication",
            "IE	 Intermittent",
            "IL	 Insulation Low",
            "IN	 Not Indicate",
            "IS	 Insulation-Damaged splice",
            "IT	 Insulation-Damaged terminal",
            "IW	 Insulation-damaged wire",
            "KR	 Key Pad Not read able",
            "LD	 Loose/detached",
            "LT	 Lost",
            "NI	 Not Illuminate",
            "NS	 Noisy",
            "OA	 Out of Adjust",
            "OC	 Open Circuit",
            "OH	 Overheat",
            "OS	 Overheating-evidence of splice",
            "OT	 Overheating-evidence of terminal",
            "OW	 Overheating-evidence of wire",
            "PI	 Not Proper Install",
            "PT	 Pitted",
            "SC	 Short Circuit",
            "SD	 Soldering Defect",
            "TB	 Circuit Breaker tripped",
            "TH	 tightened",
            "UB	 Unbonded",
            "UP	 Unplugged",
            "UR	 Unreadable",
            "UV	 Unvisible",
            "WB	 Wire-Screen braid damaged/corroded",
            "WC	 Wire Cut/Disconnected",
            "WG	 Wire Not Ground",
            "WS	 Wire-Chafe wire to structure",
            "WW	 Wire-Chafe wire to wire",
            "ZZ	 Other - defined in text"
    };

    public static String[] ZPM_CMC = {
            "AR	 Arced",
            "BA	 Battered",
            "BD	 Brinelled",
            "BE	 Bent",
            "BG	 Bulged",
            "BI	 Binding",
            "BO	 Bowed",
            "BR	 Broken",
            "BS	 Burrs",
            "BT	 Britlle",
            "BU	 Burned",
            "CA	 Carbonized",
            "CD	 Corroded",
            "CE	 Curled",
            "CF	 Chafed",
            "CH	 Checked",
            "CI	 Circuit Open",
            "CL	 Clearance Over Max",
            "CO	 Collapsed",
            "CP	 Chipped",
            "CR	 Cracked",
            "CS	 Crossed",
            "CU	 Compression Under Min",
            "DA	 Damaged",
            "DE	 Dented",
            "DI	 Dimple",
            "DO	 Dim. Over Max",
            "DP	 Deposit",
            "DR	 Distorted",
            "DS	 Disintegrated",
            "DT	 Deteriorated",
            "DU	 Dim. Under Min",
            "ER	 Eroded",
            "EX	 Extruded",
            "FD	 Fretted",
            "FI	 Finish Not To Spec",
            "FL	 Flattened Out",
            "FR	 Frayed",
            "GA	 Galled",
            "GO	 Gouged",
            "GR	 Grooved",
            "LD	 Loss Of Draw",
            "LO	 Loose",
            "LR	 Low Remaining",
            "MM	 Mis-Matched",
            "MO	 Modification",
            "MP	 Mis-Position",
            "NI	 Nicked",
            "OL	 Out Of Limit",
            "OR	 Out Of Round",
            "OX	 Oxidation",
            "PD	 Peened",
            "PE	 Peeled",
            "PI	 Pitted",
            "PL	 Plugged",
            "PM	 Part Missing",
            "PU	 Pick-Up",
            "RC	 Recondition",
            "RM	 Repair Code Max",
            "RO	 Rolled-Over",
            "RP	 Ruptured",
            "RU	 Rubbed",
            "SC	 Scratched",
            "SD	 Scored",
            "SE	 Seized",
            "SH	 Stretched",
            "SK	 Skidding",
            "SP	 Spalled",
            "ST	 Stripped",
            "TE	 Time Expired",
            "TO	 Torn",
            "TW	 Twisted",
            "WE	 Worn Excessively",
            "WO	 Worn Out",
            "WR	 Wrecked",
            "ZZ	 Other - defined in text"
    };

    public static String[] ZPM_STR = {
            "AB	 Abrasion",
            "BK	 Buckle",
            "BN	 Burn/scortch",
            "BR	 Broken",
            "BS	 Bush migration",
            "BT	 Burst",
            "CB	 Bent",
            "CH	 Chafe",
            "CO	 Corrosion",
            "CR	 Crack",
            "CZ	 Crazed",
            "DC	 Discolorization",
            "DE	 Dent",
            "DG	 disengaged",
            "DL	 Delamination / Disbond",
            "ER	 Erosion damage",
            "FO	 flattened out",
            "FR	 Fretting damage",
            "FS	 Loose/missing fastener",
            "GL	 Galling",
            "GU	 Gouge",
            "GV	 Grooved",
            "HL	 Hole/puncture",
            "IN	 Indentation",
            "LM	 Loose/Missing",
            "LS	 Lightning Strike",
            "LT	 Lost",
            "MG	 misaligned",
            "NK	 Nicked",
            "OA	 Out of Adjust",
            "OB	 Out of Balance",
            "OC	 Oil Can",
            "OP	 Over Play",
            "PT	 Pitted",
            "RU	 Rough",
            "SC	 Scratch/score",
            "SL	 Scibe Line",
            "ST	 Distorted",
            "WO	 Wear/ Worn",
            "WR	 Wrinkle",
            "ZZ	 Other-defined in text"
    };

    public static String [] ZPM_SYS = {
            "AR	 Arching",
            "BD	 Binding",
            "BF	 Bond failure",
            "BI	 BITE failure",
            "BN	 Burn/scortch",
            "BR	 Broken",
            "BT	 Burst",
            "CB	 Bent",
            "CF	 Component failure",
            "CH	 Chafe",
            "CP	 Chipped",
            "CR	 Crack",
            "CT	 Contaminated",
            "DA	 Damaged",
            "DE	 Dent",
            "DG	 Disengaged",
            "DR	 Dirty",
            "DT	 Distorted/No",
            "FB	 Fire/burnt",
            "FE	 Failed electrically",
            "FO	 Failed open",
            "FS	 Failed shut",
            "GV	 Grooved",
            "IE	 Intermittent",
            "IN	 Inoperative",
            "IO	 Inoperative",
            "JM	 Jammed",
            "LC	 Low contents/weight",
            "LD	 Loose/detached",
            "LK	 Leak",
            "LP	 Low pressure",
            "LT	 Lost",
            "MG	 Misaligned",
            "NK	 Nicked",
            "NO	 Noisy operation",
            "NS	 Not Sealed",
            "OA	 Out of Adjust",
            "OB	 Out of Balance",
            "OH	 Overheat",
            "OL	 Overload",
            "OP	 Over Play",
            "PG	 Pull Through",
            "PI	 Not Proper Install",
            "PO	 Peel Off",
            "PT	 Pitted",
            "RT	 Rejected take-off",
            "RU	 Rough",
            "SC	 Scratch/score",
            "SH	 Sheared",
            "SP	 Split/tear",
            "SR	 Striped",
            "ST	 Stiff operation",
            "TH	 tightened",
            "TX	 Timex",
            "UB	 Unbonded",
            "UP	 Unplugged",
            "US	 Unserviceable",
            "UV	 Unvisible",
            "VI	 Vibration",
            "WL	 Worn to limits",
            "ZZ	 Other - defined in text"
    };

    public static String[] ZPM_STR_CAUSE = {
            "AC  Accidental damage",
            "BL  Blockage",
            "BS  Birdstrike",
            "CH  Chafe/wear",
            "CL  Chemical leak/spill",
            "CP  CPF missing/poor",
            "EN  Environment",
            "FG  Fatigue",
            "GH  Ground handling",
            "GS  Galley/lav spill",
            "HT  High Temperature",
            "IL  Internal leak",
            "IM  Impact",
            "LS  Lighning strike",
            "MO  Moisture ingress",
            "OL  Overload",
            "QL  Quality discrepency",
            "SL  Sealant condition",
            "SP  Surface protection scratched/poor",
            "WB  Wet blanket",
            "WI  Water ingress",
            "ZZ  Other - defined in cause text"

    };

    public static String[] ZPM_STR_OBJECTPART = {
            "AC  Access/Lightening Hole",
            "BE  Beam",
            "BH  Bulkhead",
            "BO  Bond",
            "BR  Bushing",
            "CH  Chord",
            "CP  Clip",
            "CT  Cut-Out",
            "DC  Duct",
            "FA  Fastener",
            "FH  Fastener Hole",
            "FI  Fitting",
            "FL  Flap Track",
            "FO  Frame Around Opening",
            "FR  Frame",
            "JS  Joint/Splice",
            "KB  Keel Beam",
            "LG  Landing Gear Beam",
            "LO  Longeron",
            "LR  Link/Rod",
            "LU  Lig",
            "PI  Pin",
            "PL  Plate/Panel",
            "PN  Plate Nut",
            "PR  Pressure Bulkhead",
            "RD  Radome",
            "RI  Rib",
            "RT  Roller Track",
            "SK  SKIN",
            "SO  Support",
            "SP  Spar",
            "SR  Stringer",
            "SS  Stop",
            "ST  Seat Track",
            "SU  Strut/Brace",
            "TB  Tab",
            "WB  Web",
            "WD  Weld",
            "WW  Window Forging",
            "ZZ  Other - Defined In Text"
    };

    public static final ArrayList<ObjPSI> listPSI = new ArrayList<ObjPSI>(){{
        add(new ObjPSI("Y", "Default"));
        add(new ObjPSI("N", "NO"));
        add(new ObjPSI("Y", "YES"));
    }};

    public static final ArrayList<ObjStaType> listStationType = new ArrayList<ObjStaType>(){{
        add(new ObjStaType("WBL", "Wing Buttock Line"));
        add(new ObjStaType("STAB", "HoriZSF_ontal Stabiliser"));
        add(new ObjStaType("FIN", "Fin"));
        add(new ObjStaType("IFS", "Inboard Front Spar"));
        add(new ObjStaType("OFS", "Outboard Front Spar"));
        add(new ObjStaType("ILE", "Inboard Leading Edge"));
        add(new ObjStaType("OLE", "Outboard Leading Edge"));

        add(new ObjStaType("WT", "Wing Tip"));
        add(new ObjStaType("RUD", "Rudder"));
        add(new ObjStaType("FS", "Front Spar"));
        add(new ObjStaType("RS", "Rear Spar"));
        add(new ObjStaType("ELEV", "Elevator Station"));
        add(new ObjStaType("R", "Rib"));
        add(new ObjStaType("F", "Frame"));
        add(new ObjStaType("NF", "Nose Frame"));
        add(new ObjStaType("SP", "Spar"));
        add(new ObjStaType("FSP", "False Spar"));
        add(new ObjStaType("BS", "Body Station"));
        add(new ObjStaType("BBL", "Body Buttock Line"));
        add(new ObjStaType("RBL", "Right Buttock Line"));
        add(new ObjStaType("LBL", "Left Buttock Line"));

        add(new ObjStaType("DOOR", "Door"));
        add(new ObjStaType("NAC", "Nacelle"));
        add(new ObjStaType("W", "Wing"));
        add(new ObjStaType("FL", "Flap"));
        add(new ObjStaType("AE", "Aileron"));
        add(new ObjStaType("WL", "Winglet"));
        add(new ObjStaType("PY", "Pylon"));
    }};

    public static final ArrayList<ObjDefectLimit> listDefect = new ArrayList<ObjDefectLimit>(){{
        add(new ObjDefectLimit("W", "Within Approved Limits"));
        add(new ObjDefectLimit("O", "Outside Approved Limits"));
        add(new ObjDefectLimit("I", "Under Investigation"));
    }};


}
