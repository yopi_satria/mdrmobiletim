package com.gmf.mdrcreation.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.gmf.mdrcreation.R;

import net.cachapa.expandablelayout.ExpandableLayout;

public class PopupMDRcode {
    Context mContext;
    Button mMDRcode;
    String m_mdrcode = null;
    String m_subcode = null;
    LinearLayout mHidenForm;
    private EditText mTxtStaFrom, mTxtStaTo,
            mTxtStringFrom, mTxtStringTo,
            mTxtWLFrom, mTxtWLTo,
            mTxtBLFrom, mTxtBLTo,
            mTxtClock,mTxtZone,
            mTxtCauseText
                    ;

    public String getM_mdrcode() {
        return m_mdrcode;
    }

    public String getM_subcode() {
        return m_subcode;
    }

    public PopupMDRcode(Context ctx) {
        this.mContext = ctx;

        mMDRcode = ((Activity)mContext).findViewById(R.id.btnmdrcode);
        mHidenForm =  ((Activity)mContext).findViewById(R.id.hidenform);

        mTxtStaFrom = ((Activity)mContext).findViewById(R.id.txtStaFrom);
        mTxtStaTo = ((Activity)mContext).findViewById(R.id.txtStaTo);
        mTxtStringFrom = ((Activity)mContext).findViewById(R.id.txtStringFrom);
        mTxtStringTo = ((Activity)mContext).findViewById(R.id.txtStringTo);
        mTxtWLFrom = ((Activity)mContext).findViewById(R.id.txtWLFrom);
        mTxtWLTo = ((Activity)mContext).findViewById(R.id.txtWLTo);
        mTxtBLFrom = ((Activity)mContext).findViewById(R.id.txtBLFrom);
        mTxtBLTo = ((Activity)mContext).findViewById(R.id.txtBLTo);
        mTxtClock = ((Activity)mContext).findViewById(R.id.txtClock);
        mTxtZone = ((Activity)mContext).findViewById(R.id.txtZone);
        mTxtCauseText = ((Activity)mContext).findViewById(R.id.txtCauseText);


    }

    public void popupdialog(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mdr_code);

        ExpandableLayout expandableLayout = dialog.findViewById(R.id.expandable_layout);
        expandableLayout.setInterpolator(new OvershootInterpolator());
        LinearLayout avi = dialog.findViewById(R.id.avi);
        avi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expandableLayout.isExpanded()){
                    avi.setSelected(false);
                    expandableLayout.setExpanded(false, false);
                }else{
                    avi.setSelected(true);
                    expandableLayout.setExpanded(true, false);
                }
            }
        });

        ListView zpmavi = dialog.findViewById(R.id.zpmaviList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_list_item_1, android.R.id.text1, ConstantVar.ZPM_AVI);
        zpmavi.setAdapter(adapter);
        zpmavi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMDRcode.setText("ZPM-AVI -   "+ ConstantVar.ZPM_AVI[position]);
                m_mdrcode = "ZPM-AVI";
                String[] sub = ConstantVar.ZPM_AVI[position].split("\\s+");
                m_subcode = sub[0];
                //mMDRcode.setBackgroundColor(R.color.colorWhite);
                hideHiddenForm();
                dialog.dismiss();
            }
        });

        ExpandableLayout expandableLayout2 = dialog.findViewById(R.id.expandable_layout2);
        expandableLayout2.setInterpolator(new OvershootInterpolator());
        LinearLayout cmc = dialog.findViewById(R.id.cmc);
        cmc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expandableLayout2.isExpanded()){
                    cmc.setSelected(false);
                    expandableLayout2.setExpanded(false, false);
                }else{
                    cmc.setSelected(true);
                    expandableLayout2.setExpanded(true, false);
                }
            }
        });

        ListView zpmcmc = dialog.findViewById(R.id.zpmcmcList);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this.mContext,
                android.R.layout.simple_list_item_1, android.R.id.text1, ConstantVar.ZPM_CMC);
        zpmcmc.setAdapter(adapter2);
        zpmcmc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMDRcode.setText("ZPM-CMC -   "+ ConstantVar.ZPM_CMC[position]);
                m_mdrcode = "ZPM-CMC";
                String[] sub = ConstantVar.ZPM_CMC[position].split("\\s+");
                //mMDRcode.setBackgroundColor(R.color.colorWhite);
                m_subcode = sub[0];
                hideHiddenForm();
                dialog.dismiss();
            }
        });

        ExpandableLayout expandableLayout3 = dialog.findViewById(R.id.expandable_layout3);
        expandableLayout3.setInterpolator(new OvershootInterpolator());
        LinearLayout str = dialog.findViewById(R.id.str);
        str.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expandableLayout3.isExpanded()){
                    str.setSelected(false);
                    expandableLayout3.setExpanded(false, false);
                }else{
                    str.setSelected(true);
                    expandableLayout3.setExpanded(true, false);
                }

            }
        });

        ListView zpmstr = dialog.findViewById(R.id.zpmstrList);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this.mContext,
                android.R.layout.simple_list_item_1, android.R.id.text1, ConstantVar.ZPM_STR);
        zpmstr.setAdapter(adapter3);
        zpmstr.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMDRcode.setText("ZPM-STR -   "+ ConstantVar.ZPM_STR[position]);
                m_mdrcode = "ZPM-STR";
                String[] sub = ConstantVar.ZPM_STR[position].split("\\s+");
                m_subcode = sub[0];
                //mMDRcode.setBackgroundColor(R.color.colorWhite);
                showHiddenForm();
                dialog.dismiss();
            }
        });

        ExpandableLayout expandableLayout4 = dialog.findViewById(R.id.expandable_layout4);
        expandableLayout.setInterpolator(new OvershootInterpolator());
        LinearLayout sys = dialog.findViewById(R.id.sys);
        sys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expandableLayout4.isExpanded()){
                    sys.setSelected(false);
                    expandableLayout4.setExpanded(false, false);
                }else{
                    sys.setSelected(true);
                    expandableLayout4.setExpanded(true, false);
                }

            }
        });

        ListView zpmsys = dialog.findViewById(R.id.zpmsysList);
        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this.mContext,
                android.R.layout.simple_list_item_1, android.R.id.text1, ConstantVar.ZPM_SYS);
        zpmsys.setAdapter(adapter4);
        zpmsys.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMDRcode.setText("ZPM-SYS -   "+ ConstantVar.ZPM_SYS[position]);
                m_mdrcode = "ZPM-SYS";
                String[] sub = ConstantVar.ZPM_SYS[position].split("\\s+");
                m_subcode = sub[0];
                //mMDRcode.setBackgroundColor(R.color.colorWhite);
                hideHiddenForm();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void showHiddenForm(){
        //mHidenForm = (LinearLayout) findViewById(R.id.hidenform);
        mHidenForm.setVisibility(View.VISIBLE);
    }

    void hideHiddenForm(){
        //mHidenForm = (LinearLayout) findViewById(R.id.hidenform);
        mHidenForm.setVisibility(View.GONE);

        //Refresh Form
        mTxtStaFrom.setText("");
        mTxtStaTo.setText("");
        mTxtStringFrom.setText("");
        mTxtStringTo.setText("");
        mTxtWLFrom.setText("");
        mTxtWLTo.setText("");
        mTxtBLFrom.setText("");
        mTxtBLTo.setText("");
        mTxtClock.setText("");
        mTxtZone.setText("");
        mTxtCauseText.setText("");
    }
}
