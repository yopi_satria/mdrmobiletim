package com.gmf.mdrcreation.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.gmf.mdrcreation.R;

import net.cachapa.expandablelayout.ExpandableLayout;

public class PopupObjectPart {
    Context mContext;
    Button mObjPart;
    String m_mdrcode, m_subcode;


    public String getM_mdrcode() {
        return m_mdrcode;
    }

    public String getM_subcode() {
        return m_subcode;
    }
    public PopupObjectPart(Context ctx) {
        this.mContext = ctx;
        mObjPart = (Button) ((Activity)mContext).findViewById(R.id.btnobpart);
    }

    public void popupdialog(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_object_part);


        ExpandableLayout expandableLayout3 = dialog.findViewById(R.id.cs_expand_layout3);
        expandableLayout3.setInterpolator(new OvershootInterpolator());
        expandableLayout3.setExpanded(true);
        LinearLayout str = dialog.findViewById(R.id.cs_str);
        str.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expandableLayout3.isExpanded()){
                    str.setSelected(false);
                    expandableLayout3.setExpanded(false, false);
                }else{
                    str.setSelected(true);
                    expandableLayout3.setExpanded(true, false);
                }

            }
        });

        ListView zpmstr = dialog.findViewById(R.id.cs_zpmstrList);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this.mContext,
                android.R.layout.simple_list_item_1, android.R.id.text1, ConstantVar.ZPM_STR_OBJECTPART);
        zpmstr.setAdapter(adapter3);
        zpmstr.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mObjPart.setText("ZPM-STR -   "+ ConstantVar.ZPM_STR_OBJECTPART[position]);
                m_mdrcode = "ZPM-STR";
                String[] sub = ConstantVar.ZPM_STR_OBJECTPART[position].split("\\s+");
                m_subcode = sub[0];
//                showHiddenForm();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
