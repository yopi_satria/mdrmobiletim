package com.gmf.mdrcreation.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class UtilityApp {

    //Cut number dari Scan Number
    public String formatOrderScan(String number){
        //String strOrder = number.substring(4,13);
        return number.substring(4,13);
    }

    public static File convertImageToFile(Context ctx, Bitmap bitmap, String name) {
        File filesDir = ctx.getFilesDir();
        //File imageFile = new File(filesDir, name + ".jpg");
        File imageFile = new File(filesDir, name);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            return imageFile;
        } catch (Exception e) {
            Toast.makeText(ctx,e.getMessage().toString(),
                    Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        Bitmap newBitmap = null;
        float ratio = Math.max(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public static int sizeOfImage(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else if (Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT){
            return data.getByteCount();
        } else{
            return data.getAllocationByteCount();
        }
    }

}
